package tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONObject;

import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.UpdateOptions;

public class FriendsTools {
	
	public static boolean isUserFollowed(int UserWhoFollow, int UserWhoIsFollowed) {
		Document d1 = new Document("userID", UserWhoIsFollowed).append("Followers", UserWhoFollow);
		List<Document> ld = BDTools.find("Friends", d1);
		return ld.size() != 0;
	}
	
	public static List<Integer> getFollowers(int userID) throws SQLException {
		Document d = new Document().append("userID", userID);
		List<Document> ld = BDTools.find("Friends", d);
		return (List<Integer>)ld.get(0).get("Followers");
	}
	
	public static List<Integer> getFollowing(int userID) throws SQLException {
		Document d = new Document().append("userID", userID);
		List<Document> ld = BDTools.find("Friends", d);
		return (List<Integer>)ld.get(0).get("Following");
	}
	
	public static void createUser(int userID){
		// On ajoute celui qui suit à la liste des abonnés de celui qui est suivi
		Document d = new Document("userID", userID).append("Followers", new ArrayList<Integer>()).append("Following", new ArrayList<Integer>());
		BDTools.insertOne("Friends", d);
	}
	
	public static boolean userExists(int userID) {
		Document d1 = new Document("userID", userID);
		List<Document> ld = BDTools.find("Friends", d1);
		return ld.size() != 0;
	}
	
	public static void Follow(int UserWhoFollow, int UserWhoIsFollowed){
		// On ajoute celui qui suit à la liste des abonnés de celui qui est suivi
		Document d1 = new Document("Followers", UserWhoFollow);
		Document d2 = new Document("$push", d1);
		Document d3 = new Document("userID", UserWhoIsFollowed);
		UpdateOptions options = new UpdateOptions().upsert(true);
		BDTools.getCollection("Friends").updateOne(d3, d2, options);
		
		// On ajoute celui qui est suivi à la liste des abonnement de celui qui suit
		d1 = new Document("Following", UserWhoIsFollowed);
		d2 = new Document("$push", d1);
		d3 = new Document("userID", UserWhoFollow);
		BDTools.getCollection("Friends").updateOne(d3, d2);
	}
	
	public static void Unfollow(int UserWhoUnfollow, int UserWhoIsUnfollowed){
		// On ajoute celui qui suit à la liste des abonnés de celui qui est suivi
		Document d1 = new Document("Followers", UserWhoUnfollow);
		Document d2 = new Document("$pull", d1);
		Document d3 = new Document("userID", UserWhoIsUnfollowed);
		BDTools.getCollection("Friends").updateOne(d3, d2);
		
		// On ajoute celui qui est suivi à la liste des abonnement de celui qui suit
		d1 = new Document("Following", UserWhoIsUnfollowed);
		d2 = new Document("$pull", d1);
		d3 = new Document("userID", UserWhoUnfollow);
		BDTools.getCollection("Friends").updateOne(d3, d2);
	}
	
	public static void deleteUser(int userID) throws SQLException{
		List<Integer> Following = getFollowing(userID);
		List<Integer> Followers = getFollowers(userID);
		
		// On retire l'utilisateur de la liste 'Following' de chacun de ses abonnés
		for (Integer followerID : Followers) {
			Document d1 = new Document("Following", userID);
			Document d2 = new Document("$pull", d1);
			Document d3 = new Document("userID", followerID);
			BDTools.getCollection("Friends").updateOne(d3, d2);
		}
		
		// On retire l'utilisateur de la liste 'Followers' de chacun de ses abonnement
		for (Integer followingID : Following) {
			Document d1 = new Document("Followers", userID);
			Document d2 = new Document("$pull", d1);
			Document d3 = new Document("userID", followingID);
			BDTools.getCollection("Friends").updateOne(d3, d2);
		}
		
		// On supprime l'utilisateur de la BD 'Friends'
		Document d = new Document("userID", userID);
		BDTools.deleteOne("Friends", d);
	}
}
