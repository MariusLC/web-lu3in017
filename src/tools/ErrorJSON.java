package tools;

import org.json.JSONException;
import org.json.JSONObject;

public class ErrorJSON {
	
	public static JSONObject serviceRefused(String message, int codeErreur) {
		try {
			return new JSONObject().put("message", message).put("id", codeErreur);
		} catch (JSONException e) {
			// On ne fait rien
		}
		return null;
		
	}
	
	public static JSONObject serviceAccepted() {
		return new JSONObject();
	}
	
	public static String toString(JSONObject json) {
		if (!json.isNull("message")) {
			try {
				return ""+ json.get("message") + "\t" +json.get("id");
			} catch (JSONException e) {
				e.printStackTrace();
				return e.getMessage();
			}
		} else {
			return "Tache accomplie sans erreur !";
		}
	}
}
