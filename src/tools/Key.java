package tools;

import java.util.Random;

public class Key {
	
	public static String generationKey() {
		// 32 caractères aléatoires.
		Random rand = new Random();
		String s="";
		for(int i = 0 ; i < 32 ; i++){
		  char c = (char)(rand.nextInt(26) + 97);
		  s += c;
		}
		return s;
	}

}
