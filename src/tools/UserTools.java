package tools;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;

// Tous les tests et autres concernant les utilisateurs.
public class UserTools {
	
	public static boolean userExists(int userID) throws SQLException {
		// accès à la BD puis indique si l'utilisateur est connecté.
		ResultSet res = BDTools.exeQuery("SELECT * FROM User WHERE userID = '" + userID + "'");
		return res.next();
	}
	
	public static boolean LoginAlreadyExists(String login) throws SQLException {
		// accès à la BD puis retourne un boolean.
		ResultSet res = BDTools.exeQuery("SELECT * FROM User WHERE login = '" + login+"'");
		return res.next();
	}
	
	public static boolean passwordMatchsLogin(String login, String password) throws SQLException {
		// accès à la BD pour voir si le mdp correspond au login.
		ResultSet res = BDTools.exeQuery("SELECT * FROM User WHERE login = '" + login + "' AND password = '" + password+"'");
		return res.next();
	}
	
	public static int getIdUser(String login) throws SQLException {
		// accès à la BD puis retourne l'ID de l'utilisateur.
		ResultSet res = BDTools.exeQuery("SELECT userID FROM User WHERE login = '" + login+"'");
		boolean b = res.next();
		if(b) return res.getInt("userId");
		else return -1;
	}
	
	public static String getLogin(int userID) throws SQLException {
		// accès à la BD puis retourne l'ID de l'utilisateur.
		ResultSet res = BDTools.exeQuery("SELECT login FROM User WHERE userID = '" + userID+"'");
		res.next();
		return res.getString("login");
	}
	
	public static boolean IsUserConnected(int userID) throws SQLException {
		// accès à la BD puis indique si l'utilisateur est connecté.
		ResultSet res = BDTools.exeQuery("SELECT * FROM Sessions WHERE userID = '" + userID + "'");
		return res.next();
	}
	
	public static boolean checkSession(int userID) throws SQLException {
		ResultSet res = BDTools.exeQuery("SELECT * FROM Sessions WHERE userID = '" + userID + "'");
		boolean connected = false;
		if (!res.next()) {
			// L'Utilisateur ne possède pas de clé de session... Il n'est pas connecté..
			// Error ???
		} else {
			//L'Utilisateur possède une clé de session, vérifions si elle est valide.
			ResultSet res2 = BDTools.exeQuery("SELECT * FROM Sessions WHERE userID = '" + userID + "' AND ADDTIME(date,\"3000\") > CURRENT_TIMESTAMP");
			if (!res2.next()) {
				// La clé n'est plus valide, on doit déconnecter l'Utilisateur.
				UserTools.deleteSession(userID);
			} else {
				// La clé est valide, on la met à jour
				String update = " UPDATE Sessions SET date = CURRENT_TIME WHERE userID = '"+userID+"'";
				int i = BDTools.exeUpdate(update);
				connected = true;
			}
		}
		return connected;
	}
	
	public static String insertSession(int userID, boolean b) throws SQLException /* b = admin ? */{
		ResultSet res = BDTools.exeQuery("SELECT * FROM Sessions WHERE userID = '" + userID + "'");
		String key;
		if (!res.next()) {
			key = Key.generationKey();
			// Insérer dans la bd : key = userId, valeur = key.
			String ins = " INSERT INTO Sessions (userID, clef, date) VALUES ('" + userID + "', '" + key + "', CURRENT_TIME)";
			int i = BDTools.exeUpdate(ins);
		} else {
			// L'Utilisateur a déjà une clé, très bizarre... Error ?
			key = res.getString("clef");
			String ins = " UPDATE Sessions SET date = CURRENT_TIME WHERE userID = '"+userID+"'";
			int i = BDTools.exeUpdate(ins);
		}
		return key;
	}
	
	public static boolean deleteSession(int userID) throws SQLException {
		ResultSet res = BDTools.exeQuery("SELECT clef FROM Sessions WHERE userID = '" + userID + "'");
		if(res.next()) {
			// Supprimer la key d'authentification active dans la base de donnée.
			String ins = " DELETE FROM Sessions WHERE userID = '" + userID + "'";
			int i = BDTools.exeUpdate(ins);
			return true;
		}
		return false;
	}
}
