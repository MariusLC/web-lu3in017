package tools;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import bd.*;

public class BDTools {
	
////---------------- SQL ---------------------
	
	public static ResultSet exeQuery(String sql) throws SQLException {
		Connection c = Database.getMySQLConnection();
		Statement s = c.createStatement();
		ResultSet res = s.executeQuery(sql);
		return res;
	}
	
	public static int exeUpdate(String sql) throws SQLException {
		Connection c = Database.getMySQLConnection();
		Statement s = c.createStatement();
		int res = s.executeUpdate(sql);
		return res;
	}	
	
	////  ---------------- MONGO DB ---------------------
	
	public static MongoCollection<Document> getCollection(String collection){
		MongoDatabase db = Database.getMongoDBConnection();
		return db.getCollection(collection);
	}
	
	public static List<Document> find(String collection, Document d) {
		MongoCursor<Document> c = getCollection(collection).find(d).iterator();
		ArrayList<Document> l = new ArrayList<Document>();
		while(c.hasNext()) {
			l.add(c.next());
		}
		return l;
	}
	
	public static void insertMany(String collection, List<Document> ld) {
		getCollection(collection).insertMany(ld);
	}
	
	public static void insertOne(String collection, Document d) {
		getCollection(collection).insertOne(d);
	}
	
//	public static void updateOne(String messageID, String collection, Document d) {
//		Document d2 = new Document("_id", new ObjectId(messageID));
//		getCollection(collection).updateOne(d2, d);
//	}
//	
//	public static void updateMany(String messageID, String collection, Document d) {
//		Document d2 = new Document("_id", new ObjectId(messageID));
//		getCollection(collection).updateMany(d2, d);
//	}
	
	public static void deleteOne(String collection, Document d) {
		getCollection(collection).deleteOne(d);
	}
	
	public static void deleteMany(String collection, Document d) {
		getCollection(collection).deleteMany(d);
	}
	
}
