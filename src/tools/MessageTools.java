package tools;

import java.sql.SQLException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;


import bd.Database;

public class MessageTools {
	
	public static boolean messageExists(String messageID) {
		Document d1 = new Document("_id", new ObjectId(messageID));
		List<Document> ld = BDTools.find("messages", d1);
		return ld.size() != 0;
	}
	
	public static boolean commentExists(String messageID, String commentID) {
		Document d1 = new Document("_id", new ObjectId(messageID)).append("comments._id", new ObjectId(commentID));
		List<Document> ld = BDTools.find("messages", d1);
		return ld.size() != 0;
	}
	
	public static boolean messageIsFromUser(String messageID, int userID) {
		Document d1 = new Document("_id", new ObjectId(messageID)).append("userID", userID);
		List<Document> ld = BDTools.find("messages", d1);
		return ld.size() != 0;
	}
	
	public static boolean commentIsFromUser(String messageID, String commentID, int userID) {
		Document d1 = new Document("_id", new ObjectId(messageID)).append("comments._id", new ObjectId(commentID)).append("comments.userID", userID);
		List<Document> ld = BDTools.find("messages", d1);
		return ld.size() != 0;
	}
	
	public static boolean isLikedByUser(String messageID, int userID) {
		Document d1 = new Document("_id", new ObjectId(messageID)).append("likes", userID);
		List<Document> ld = BDTools.find("messages", d1);
		return ld.size() != 0;
	}
	
	public static List<Document> getMessages(String login) throws SQLException {
		int userID = UserTools.getIdUser(login);
		Document d = new Document().append("userID", userID);
		List<Document> ld = BDTools.find("messages", d);
		return ld;
	}
	
	public static boolean likeMessage(String messageID, int userID) {
		Document d1 = new Document("likes", userID);
		Document d2 = new Document("$push", d1);
		Document d3 = new Document("_id", new ObjectId(messageID));
		BDTools.getCollection("messages").updateOne(d3, d2);
		return true;
	}
	
	public static boolean unlikeMessage(String messageID, int userID) {
		Document d1 = new Document("likes", userID);
		Document d2 = new Document("$pull", d1);
		Document d3 = new Document("_id", new ObjectId(messageID));
		BDTools.getCollection("messages").updateOne(d3, d2);
		return true;
	}
	
	public static boolean postComment(String messageID, Document d) {
		Document d1 = new Document("comments", d);
		Document d2 = new Document("$push", d1);
		Document d3 = new Document("_id", new ObjectId(messageID));
		BDTools.getCollection("messages").updateOne(d3, d2);
		return true;
	}
	
	public static Date getCurrentDate() {
		GregorianCalendar c = new GregorianCalendar();
		return c.getTime();
	}
	
}
