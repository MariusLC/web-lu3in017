package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import bd.Database;

public class ServletBd extends HttpServlet {
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter writer = res.getWriter();
		writer.println("Acces BD");
		
		try {
			Connection c = Database.getMySQLConnection();
			Statement s = c.createStatement();
			
			String sql = "SELECT * FROM User;";
			ResultSet resSet = s.executeQuery(sql);
			while(resSet.next()) {
				writer.println(resSet.getString("prenom") +" "+ resSet.getString("nom") +" AKA "+ resSet.getString("login") + ", userID : " + resSet.getString("userID"));
			}
			
//			String ins = " INSERT INTO User (userID, prenom, nom, login, password) VALUES (NULL, 'bruce', 'wayne', 'batman', 'batman'), (NULL, 'clark', 'kent', 'superman', 'superman')";
//			int i = s.executeUpdate(ins);
//			
//			resSet = s.executeQuery(sql);
//			while(resSet.next()) {
//				writer.println(resSet.getString("prenom") +" "+ resSet.getString("nom") +" AKA "+ resSet.getString("login") + ", userID : " + resSet.getString("userID"));
//			}
			
			s.close();
			c.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
			writer.println("Exception");
		}
	}
	
}
