package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import services.*;
import tools.ErrorJSON;

public class ServletDeleteUser extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter writer = res.getWriter();
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title> Delete User </title>");
		writer.println("</head>");
		writer.println("<body>");
		writer.println("<form method='post' action='deleteUser'>\n");
		writer.println("UserID : <input type='text' name='userID' /><br  />");
		writer.println("Password : <input type='text' name='password' /><br  />");
		writer.println("<input type='submit' name='btn' /><br />");
		writer.println("</form>");
		writer.println("</body>");
		writer.println("</html>");
		
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter writer = res.getWriter();
		String userID_String = req.getParameter( "userID" );
		String password = req.getParameter( "password" );
		int userID = -1;
		try {
			userID = Integer.parseInt(userID_String);
		} catch (NumberFormatException e ){
			writer.println(ErrorJSON.serviceRefused("Erreur : UserID incorrect " + userID_String, 1));
		}
		writer.println(UserServices.deleteUser(userID, password));
	}
}
