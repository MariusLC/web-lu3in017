package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import services.*;
import tools.ErrorJSON;

public class ServletLogin extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter writer = res.getWriter();
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title> Login </title>");
		writer.println("</head>");
		writer.println("<body>");
		writer.println("<form method='post' action='login'>\n");
		writer.println("Login : <input type='text' name='login' /><br  />");
		writer.println("Password : <input type='text' name='password' /><br  />");
		writer.println("<input type='submit' name='btn' /><br />");
		writer.println("</form>");
		writer.println("</body>");
		writer.println("</html>");
		
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		String login = req.getParameter( "login" );
		String password = req.getParameter( "password" );
		
		PrintWriter writer = res.getWriter();
		writer.println(UserServices.login(login, password));
	}
}
