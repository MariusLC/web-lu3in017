package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import services.*;
import tools.ErrorJSON;

public class ServletFollowFriend extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter writer = res.getWriter();
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title> Follow </title>");
		writer.println("</head>");
		writer.println("<body>");
		writer.println("<form method='post' action='follow'>\n");
		writer.println("yourID : <input type='text' name='yourID' /><br  />");
		writer.println("hisID : <input type='text' name='hisID' /><br  />");
		writer.println("<input type='submit' name='btn' /><br />");
		writer.println("</form>");
		writer.println("</body>");
		writer.println("</html>");
		
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter writer = res.getWriter();
		String yourID_String = req.getParameter( "yourID" );
		String hisID_String = req.getParameter( "hisID" );
		int hisID = -1;
		int yourID = -1;
		try {
			hisID = Integer.parseInt(hisID_String);
			yourID = Integer.parseInt(yourID_String);
		} catch (NumberFormatException e ){
			writer.println(ErrorJSON.serviceRefused("Erreur : UserIDs incorrects" + hisID + " : " +yourID, 1));
		}
		writer.println(FriendsServices.Follow(yourID, hisID));
	}
}
