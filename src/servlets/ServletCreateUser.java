package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import services.*;
import tools.ErrorJSON;

public class ServletCreateUser extends HttpServlet{
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter writer = res.getWriter();
		writer.println("<html>");
		writer.println("<head>");
		writer.println("<title> Create User </title>");
		writer.println("</head>");
		writer.println("<body>");
		writer.println("<form method='post' action='createUser'>\n");
		writer.println("Login : <input type='text' name='login' /><br  />");
		writer.println("Password : <input type='text' name='password' /><br  />");
		writer.println("Nom : <input type='text' name='nom' /><br  />");
		writer.println("Prenom : <input type='text' name='prenom' /><br  />");
		writer.println("<input type='submit' name='btn' /><br />");
		writer.println("</form>");
		writer.println("</body>");
		writer.println("</html>");
		
	}
	
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		PrintWriter writer = res.getWriter();
		String login = req.getParameter( "login" );
		String password = req.getParameter( "password" );
		String nom = req.getParameter( "nom" );
		String prenom = req.getParameter( "prenom" );
		writer.println(UserServices.createUser(login, password, prenom, nom));
		
	}
}
