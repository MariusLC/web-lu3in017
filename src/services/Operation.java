package services;

public class Operation {
	
	public static double addition(double a, double b) {
		return a+b;
	}
	
	public static double multiplication(double a, double b) {
		return a*b;
	}
	
	public static double division(double a, double b) {
		return a/b;
	}
	
	public static double calcul(double a, double b, String operation) {
		if (operation == "add") {
			return addition(a,b);
		} else if (operation == "mult") {
			return multiplication(a,b);
		} else if (operation == "div") {
			return division(a,b);
		} else {
			System.out.println("ERROR not an operation");
		}
		return -1; // null incompatible avec double
	}
}
