package services;

import java.sql.SQLException;

import org.json.JSONException;
import org.json.JSONObject;
import tools.*;

public class UserServices {
	
	public static JSONObject createUser(String login, String password, String prenom, String nom) {
		// test arguments web Service (arguments manquants, mauvais format...)
		try {
			if(login == null || password == null || prenom == null || nom == null) { 
				return ErrorJSON.serviceRefused("Erreur : arguments manquants", 1); // erreur : argument manquant
			}
			// test si le login existe déjà
			else if (UserTools.LoginAlreadyExists(login)){
				return ErrorJSON.serviceRefused("Erreur : login existe", 2); // erreur : le login existe déjà
			}
			// Tous les tests sont passés avec succès, on renvoie un JSON service Accepted !
			else {
				// Création du User dans la BD avec une requete SQL :
				// retourne un jSON vide pour dire que tout est OK
				String ins = " INSERT INTO User (userID, prenom, nom, login, password) VALUES (NULL, '"+prenom+"', '"+nom+"', '"+login+"', '"+password+"')";
				int i = BDTools.exeUpdate(ins);
				
//				// Création du User dans la collection "Friends" de la BD mongoDB
//				int userID = UserTools.getIdUser(login);
//				FriendsTools.createUser(userID);
				
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			// Gestion BD Exception
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		} catch (Exception e) {
			// Gestion autres Exceptions
			return ErrorJSON.serviceRefused("Problem ... "+e.getMessage(), 10000);
		}
	}
	
	public static JSONObject deleteUser(int userID, String password) {
		// test arguments web Service (arguments manquants, mauvais format...)
		try {
			String login = UserTools.getLogin(userID);
			// test si le login n'existe pas
			if (!UserTools.LoginAlreadyExists(login)){
				return ErrorJSON.serviceRefused("Erreur : login existe", 2); // erreur : le login existe déjà
			// test si le password est bon pour le login
			} else if (!UserTools.passwordMatchsLogin(login, password)) {
				return ErrorJSON.serviceRefused("Erreur : login ou mot de passe invalide", 3); // erreur : le mdp ne correspond pas au login
			// Tous les tests sont passés avec succès, on renvoie un JSON service Accepted !
			} else {
				if (FriendsTools.userExists(userID)) {
					// Suppression de l'utilisateur de la BD 'Friends'
					FriendsTools.deleteUser(userID);
				}
				// Suppression du User dans la BD avec une requete SQL :
				// retourne un jSON vide pour dire que tout est OK
				String ins = "DELETE FROM User WHERE login = '" + login + "'";
				int i = BDTools.exeUpdate(ins);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			// Gestion BD Exception
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		} catch (Exception e) {
			// Gestion autres Exceptions
			return ErrorJSON.serviceRefused("Problem ... "+e.getMessage(), 10000);
		}
	}
	
	public static JSONObject login(String login, String password) {
		try {
			// test arguments web Service (arguments manquants, mauvais format...)
			if(login == null || password == null) {
				return ErrorJSON.serviceRefused("Erreur : arguments manquants", 1); // erreur : argument manquant
			}
			// test si le login existe
			else if (!UserTools.LoginAlreadyExists(login)){
				return ErrorJSON.serviceRefused("Erreur : Le login n'existe pas", 2); // erreur : le login n'existe pas
			// test si le password est bon pour le login
			} else if (!UserTools.passwordMatchsLogin(login, password)) {
				return ErrorJSON.serviceRefused("Erreur : login ou mot de passe invalide", 3); // erreur : le mdp ne correspond pas au login
			}
			// Tous les tests sont passés avec succès, on renvoie un JSON service Accepted !
			else {
				// login du user dans la BD avec une requete SQL :
				// retourne un jSON vide pour dire que tout est OK
				int userID = UserTools.getIdUser(login);
				String key = UserTools.insertSession(userID, false);
				return new JSONObject().put("key", key);
			}
		} catch (SQLException e) {
			// Gestion SQL Exception
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		} catch (Exception e) {
			// Gestion autres Exceptions
			return ErrorJSON.serviceRefused("Problem ... "+e.getMessage(), 10000);
		}
	}
	
	public static JSONObject logout(int userID) {
		try {
			// test arguments web Service (arguments manquants, mauvais format...)
			// test si le mdp correspond au login
			if (!UserTools.IsUserConnected(userID)) {
				return ErrorJSON.serviceRefused("Erreur : utilisateur non connecté", 2); // erreur : l'utilisateur n'est pas connecté
			}
			// Tous les tests sont passés avec succès, on renvoie un JSON service Accepted !
			else {
				// logout du user dans la BD avec une requete SQL :
				// retourne un jSON vide pour dire que tout est OK
				UserTools.deleteSession(userID);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			// Gestion SQL Exception
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		} catch (Exception e) {
			// Gestion autres Exceptions
			return ErrorJSON.serviceRefused("Problem ... "+e.getMessage(), 10000);
		}
	}
}
