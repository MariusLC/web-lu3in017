package services;

import java.util.Date;
import java.util.GregorianCalendar;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;


import tools.BDTools;
import tools.ErrorJSON;
import tools.FriendsTools;
import tools.MessageTools;
import tools.UserTools;

public class MessageServices {
	
	public static JSONObject postMessage(int userID, String text){
		// Verifier que l'utilisateur existe
		try {
			if (!UserTools.checkSession(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'est plus connecté, il doit se reconnecter", 1);
			} if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas", 2);
			} else {
				Document d = new Document("userID", userID).append("text", text).append("date", MessageTools.getCurrentDate());
				d.append("likes", new ArrayList<Document>()).append("comments", new ArrayList<Document>())/*.append("login", UserTools.getLogin(userID))*/;
				BDTools.insertOne("messages", d);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject deleteMessage(int userID, String messageID){
		// Verifier que le message existe et qu'il appartient à cet utilisateur
		try {
			if (!UserTools.checkSession(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'est plus connecté, il doit se reconnecter", 1);
			} if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas", 2);
			} else if (!MessageTools.messageExists(messageID)) {
				return ErrorJSON.serviceRefused("Erreur : le message n'existe pas", 3);
			} else if (!MessageTools.messageIsFromUser(messageID, userID)){
				return ErrorJSON.serviceRefused("Erreur : le message n'appartient pas à l'utilisateur", 4);
			} else {
				Document d = new Document("_id", new ObjectId(messageID));
				BDTools.deleteOne("messages", d);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject likeMessage(int userID, String messageID){
		// Verifier que le message et l'utilisateur existent et qu'il n'est pas déjà liké par cet utilisateur
		try {
			if (!UserTools.checkSession(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'est plus connecté, il doit se reconnecter", 1);
			} if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas", 2);
			} else if (!MessageTools.messageExists(messageID)) {
				return ErrorJSON.serviceRefused("Erreur : le message n'existe pas", 3);
			} else if (MessageTools.isLikedByUser(messageID, userID)){
				return ErrorJSON.serviceRefused("Erreur : le message déjà liké par cet utilisateur", 4);
			} else {
				MessageTools.likeMessage(messageID, userID);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject unlikeMessage(int userID, String messageID){
		// Verifier que le message existe et est bien liké par cet utilisateur
		try {
			if (!UserTools.checkSession(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'est plus connecté, il doit se reconnecter", 1);
			} if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas", 2);
			} else if (!MessageTools.messageExists(messageID)) {
				return ErrorJSON.serviceRefused("Erreur : le message n'existe pas", 3);
			} else if (!MessageTools.isLikedByUser(messageID, userID)){
				return ErrorJSON.serviceRefused("Erreur : le message n'est pas liké par cet utilisateur", 4);
			} else {
				MessageTools.unlikeMessage(messageID, userID);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject postComment(int userID, String text, String messageID){
		// Verifier que l'utilisateur et le message existent
		try {
			if (!UserTools.checkSession(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'est plus connecté, il doit se reconnecter", 1);
			} if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas", 2);
			} else if (!MessageTools.messageExists(messageID)) {
				return ErrorJSON.serviceRefused("Erreur : Le message n'existe pas", 3);
			} else {
				Document d = new Document("_id", new ObjectId()).append("userID", userID).append("text", text).append("date", MessageTools.getCurrentDate());
				/*d.append("likes", new ArrayList<Document>()).append("comments", new ArrayList<Document>()).append("login", UserTools.getLogin(userID))*/;
				MessageTools.postComment(messageID, d);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject deleteComment(int userID, String messageID, String commentID){
		// Verifier que ce commentaire existe, qu'il est bien sous ce message, et qu'il appartient bien à cet utilisateur
		try {
			if (!UserTools.checkSession(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'est plus connecté, il doit se reconnecter", 1);
			} if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas", 2);
			} else if (!MessageTools.messageExists(messageID)) {
				return ErrorJSON.serviceRefused("Erreur : Le message n'existe pas", 3);
			} else if(!MessageTools.commentExists(messageID, commentID)) {
				return ErrorJSON.serviceRefused("Erreur : Le commentaire n'existe pas", 4);
			} else {
				Document d = new Document("_id", new ObjectId(commentID));
				Document d1 = new Document("comments", d);
				Document update = new Document("$pull", d1);
				Document find = new Document("_id", new ObjectId(messageID));
				BDTools.getCollection("messages").updateOne(find, update);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject getMessageFromUser(int userID){
		// Verifier que l'utilisateur existe
		try {
			if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas", 1);
			} else {
				Document find = new Document("userID", userID);
				List<Document> listMessages = BDTools.find("messages", find);
				try {
					return new JSONObject().put("messages", listMessages);
				} catch (JSONException e) {
					e.printStackTrace();
					return ErrorJSON.serviceRefused("???", 99);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject getMessageFromFollowers(int userID){
		// Verifier que l'utilisateur existe
		try {
			if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas", 1);
			} else {
				Document find = new Document();
				List<Integer> followers = FriendsTools.getFollowers(userID);
				List<Document> listMessages = new ArrayList<Document>();
		        find.append("userID", new Document("$in", followers)); 
				GregorianCalendar calendar = new GregorianCalendar();
				calendar.add(Calendar.HOUR, -1);
				Date dateavant = calendar.getTime ();
				find.append("date", new Document("$gte", dateavant));
				listMessages.addAll(BDTools.find("messages", find));
				try {
					return new JSONObject().put("messages", listMessages);
				} catch (JSONException e) {
					e.printStackTrace();
					return ErrorJSON.serviceRefused("error json return", 99);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
}
