package services;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.json.JSONObject;


import tools.BDTools;
import tools.ErrorJSON;
import tools.FriendsTools;
import tools.MessageTools;
import tools.UserTools;

public class FriendsServices {
	
	public static JSONObject Follow(int UserWhoFollow, int UserWhoIsFollowed){
		// Verifier que les utilisateurs existent
		try {
			if (!UserTools.checkSession(UserWhoFollow)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'est plus connecté, il doit se reconnecter", 1);
			} if (!UserTools.userExists(UserWhoFollow)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur qui veut suivre n'existe pas dans la BD SQL", 2);
			} if (!UserTools.userExists(UserWhoIsFollowed)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur qui est suivi n'existe pas dans la BD SQL", 3);
			} if (FriendsTools.isUserFollowed(UserWhoFollow, UserWhoIsFollowed)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur suit déjà l'autre utilisateur", 4);
			} else {
				if (!FriendsTools.userExists(UserWhoFollow)) {
					// Création du User dans la collection "Friends" de la BD mongoDB
					FriendsTools.createUser(UserWhoFollow);
				} 
				if (!FriendsTools.userExists(UserWhoIsFollowed)) {
					// Création du User dans la collection "Friends" de la BD mongoDB
					FriendsTools.createUser(UserWhoIsFollowed);
				}
				FriendsTools.Follow(UserWhoFollow, UserWhoIsFollowed);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject Unfollow(int UserWhoUnfollow, int UserWhoIsUnfollowed){
		// Verifier que les utilisateurs existent, et que l'utilisateur qui veut unfollow, follow bien l'autre.
		try {
			if (!UserTools.checkSession(UserWhoUnfollow)) {
			return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'est plus connecté, il doit se reconnecter", 1);
			} if (!UserTools.userExists(UserWhoUnfollow)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur qui ne veut plus suivre n'existe pas dans la BD SQL", 2);
			} if (!FriendsTools.userExists(UserWhoUnfollow)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur qui ne veut plus suivre n'existe pas dans la BD Mongo Friends", 3);
			} if (!UserTools.userExists(UserWhoIsUnfollowed)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur qui est suivi n'existe pas dans la BD SQL", 4);
			} if (!FriendsTools.userExists(UserWhoIsUnfollowed)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur qui est suivi n'existe pas dans la BD Mongo Friends", 5);
			} if (!FriendsTools.isUserFollowed(UserWhoUnfollow, UserWhoIsUnfollowed)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur ne suit pas l'autre utilisateur", 6);
			} else {
				FriendsTools.Unfollow(UserWhoUnfollow, UserWhoIsUnfollowed);
				return ErrorJSON.serviceAccepted();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
	public static JSONObject getFollowers(int userID){
		try {
			if (!UserTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas dans la BD SQL", 1);
			} if (!FriendsTools.userExists(userID)) {
				return ErrorJSON.serviceRefused("Erreur : L'utilisateur n'existe pas dans la BD Mongo Friends", 2);
			} else {
				try {
					return new JSONObject().put("followers", FriendsTools.getFollowers(userID));
				} catch (JSONException e) {
					e.printStackTrace();
					return ErrorJSON.serviceRefused("???", 99);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return ErrorJSON.serviceRefused("SQL Problem "+e.getMessage(), 1000);
		}
	}
	
}
