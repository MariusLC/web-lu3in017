package test;

import services.*;
import tools.ErrorJSON;


import bd.*;

public class testUser {
	
	public static void main(String[] args) {
		
		// -------- Tests Login
		System.out.println(ErrorJSON.toString(UserServices.login("batman", null)));
		// TEST json avec erreur 1
		System.out.println(ErrorJSON.toString(UserServices.login("feaf", "aefaefa")));
		// Test json avec erreur 2
		System.out.println(ErrorJSON.toString(UserServices.login("batman", "superman")));
		// Test json avec erreur 3
		System.out.println(ErrorJSON.toString(UserServices.login("batman", "batman")));
		// Test json sans erreur 
		
		// --------- Tests CreateUser
		System.out.println(ErrorJSON.toString(UserServices.createUser(null, "batman", "bruce", "wayne")));
		// Test json avec erreur 1
		System.out.println(ErrorJSON.toString(UserServices.createUser("batman", "batman", "bruce", "wayne")));
		// Test json avec erreur 2
		System.out.println(ErrorJSON.toString(UserServices.createUser("faefae", "feafa", "faefa", "feafef")));
		// Test json sans erreur
		
		// -------- Tests Logout
//		System.out.println(ErrorJSON.toString(UserServices.logout(null)));
//		// TEST json avec erreur 1
//		System.out.println(ErrorJSON.toString(UserServices.logout("deabdi")));
//		// Test json avec erreur 2
//		System.out.println(ErrorJSON.toString(UserServices.logout("batman")));
		// Test json sans erreur
	}
}
