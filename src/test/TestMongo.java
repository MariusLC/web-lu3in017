package test;

import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import services.MessageServices;
import tools.BDTools;
import tools.ErrorJSON;
import tools.MessageTools;

public class TestMongo {
	public static void main(String[] args) {
		
//		for (int i=0; i<5; i++) {
//			MessageServices.postMessage(i, "le post n*"+i);
//		}
		
		int correctUserID = 1;
		int falseUserID = 2;
		int incorrectUserID = 17;
		
//		MessageServices.postMessage(correctUserID, "le post n*"+correctUserID);
		
		String trueMessageID = "5e8db25014f21b23a697f3bb";
		String falseMessageID = "5e8db25114f21b23a697f3b3";
		
		String trueCommentID = "5e57be4269f7671f4cd2110e";
		String falseCommentID = "5e57be4269f7671f4cd2110A";
		
		String res = "";
		String text = "salut tout le monde !";
		
		String ID = "5e8db25014f21b23a697f3bb";
		MessageServices.postComment(correctUserID, text, ID);
		
	// --- Test Post Message ---
		res += ErrorJSON.toString(MessageServices.postMessage(incorrectUserID, text));
		// L'utilisateur n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.postMessage(correctUserID, text));
		// Tache accomplie avec succès !
		
	// --- Test Like Message ---
		res += "\n" + ErrorJSON.toString(MessageServices.likeMessage(incorrectUserID, trueMessageID));
		// utilisateur n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.likeMessage(correctUserID, falseMessageID));
		// message n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.likeMessage(correctUserID, trueMessageID));
		// Tache accomplie avec succès !
		res += "\n" + ErrorJSON.toString(MessageServices.likeMessage(correctUserID, trueMessageID));
		// l'utilisateur a déjà liké ce message 
		
	// --- Test Unlike Message ---
		res += "\n" + ErrorJSON.toString(MessageServices.unlikeMessage(incorrectUserID, trueMessageID));
		// utilisateur n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.unlikeMessage(correctUserID, falseMessageID));
		// message n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.unlikeMessage(falseUserID, trueMessageID));
		// l'utilisateur ne like pas ce message
		res += "\n" + ErrorJSON.toString(MessageServices.unlikeMessage(correctUserID, trueMessageID));
		// Tache accomplie avec succès !
		
	// --- Test Post Comment ---
		res += "\n" + ErrorJSON.toString(MessageServices.postComment(incorrectUserID, text, trueMessageID));
		// utilisateur n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.postComment(correctUserID, text, falseMessageID));
		// message n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.postComment(correctUserID, text, trueMessageID));
		// Tache accomplie avec succès !
//			
	// --- Test Delete Comment ---
		res += "\n" + ErrorJSON.toString(MessageServices.deleteComment(incorrectUserID, trueMessageID, trueCommentID));
		// utilisateur n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.deleteComment(correctUserID, falseMessageID, trueCommentID));
		// message n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.deleteComment(correctUserID, trueMessageID, falseCommentID));
		// le commentaire n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.deleteComment(falseUserID, trueMessageID, trueCommentID));
		// le commentaire n'appartient pas à l'utilisateur
		res += "\n" + ErrorJSON.toString(MessageServices.deleteComment(correctUserID, trueMessageID, trueCommentID));
		// Tache accomplie avec succès !
				
	// --- Test Delete Message ---
		res += "\n" + ErrorJSON.toString(MessageServices.deleteMessage(incorrectUserID, trueMessageID));
		// utilisateur n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.deleteMessage(correctUserID, falseMessageID));
		// message n'existe pas
		res += "\n" + ErrorJSON.toString(MessageServices.deleteMessage(falseUserID, trueMessageID));
		// message n'appartient pas à l'utilisateur
		res += "\n" + ErrorJSON.toString(MessageServices.deleteMessage(correctUserID, trueMessageID));
		// Tache accomplie avec succès !
		
//		res += ErrorJSON.toString(MessageServices.likeMessage(101, "5e57be4269f7671f4cd2110f"));
		
		
	// ----- PRINT -----
		
		Document d1 = new Document().append("userID", 1);
		Document d2 = new Document().append("userID", 2);
		
		List<Document> ld1 = BDTools.find("messages", d1);
		List<Document> ld2 = BDTools.find("messages", d2);
		
		System.out.println(ld1);
		System.out.println(ld2);
		
		System.out.println(res);
	}
}