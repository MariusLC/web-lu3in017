package test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bd.*;

public class testbd {
	public static void main(String[] args) {
		try {
			Connection c = Database.getMySQLConnection();
			Statement s = c.createStatement();
			
			String sql = "SELECT * FROM User;";
			ResultSet resSet = s.executeQuery(sql);
			while(resSet.next()) {
				System.out.println(resSet.getString("prenom") +" "+ resSet.getString("nom") +" AKA "+ resSet.getString("login") + ", userID : " + resSet.getString("userID"));
			}
			
			String ins = "INSERT INTO User (login, password, prenom, nom) VALUES ('feafea', 'efafe', 'françois', 'Dupont')";
			int i = s.executeUpdate(ins);
			
			resSet = s.executeQuery(sql);
			while(resSet.next()) {
				System.out.println(resSet.getString("prenom") +" "+ resSet.getString("nom") +" AKA "+ resSet.getString("login") + ", userID : " + resSet.getString("userID"));
			}
			
			s.close();
			c.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Exception");
		}
	}
}
